package id.bima.diver.notebookshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.notebookshop.dto.NotebookTransactionHistoryDto;
import id.bima.diver.notebookshop.entity.NotebookTransactionHistory;
import id.bima.diver.notebookshop.repository.NotebookTransactionHistoryRepository;
import id.bima.diver.notebookshop.util.NotebookTransactionHistoryDtoUtil;

@Service
public class NotebookTransactionHistoryService extends DefaultDatabaseService<NotebookTransactionHistoryRepository, NotebookTransactionHistoryDtoUtil, NotebookTransactionHistoryDto, NotebookTransactionHistory, Long> {
	
	@Autowired
	public NotebookTransactionHistoryService(NotebookTransactionHistoryRepository repository, NotebookTransactionHistoryDtoUtil util) {
		super(repository, util);
	}

}
