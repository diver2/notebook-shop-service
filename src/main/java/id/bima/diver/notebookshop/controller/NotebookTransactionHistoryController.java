package id.bima.diver.notebookshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.notebookshop.constant.NotebookShopPath;
import id.bima.diver.notebookshop.dto.NotebookTransactionHistoryDto;
import id.bima.diver.notebookshop.entity.NotebookTransactionHistory;
import id.bima.diver.notebookshop.repository.NotebookTransactionHistoryRepository;
import id.bima.diver.notebookshop.service.NotebookTransactionHistoryService;
import id.bima.diver.notebookshop.util.NotebookTransactionHistoryDtoUtil;

@RestController
@RequestMapping(NotebookShopPath.NOTEBOOK_TRANSACTION_HISTORY_V1)
public class NotebookTransactionHistoryController extends DefaultDatabaseController<NotebookTransactionHistoryService, NotebookTransactionHistoryRepository, NotebookTransactionHistoryDtoUtil, NotebookTransactionHistoryDto, NotebookTransactionHistory, Long> {

	@Autowired
	protected NotebookTransactionHistoryController(NotebookTransactionHistoryService service) {
		super(service);
	}

}
