package id.bima.diver.notebookshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients
@ComponentScan(basePackages = { "id.bima.diver" })
public class NotebookShopServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotebookShopServiceApplication.class, args);
	}
	
}
