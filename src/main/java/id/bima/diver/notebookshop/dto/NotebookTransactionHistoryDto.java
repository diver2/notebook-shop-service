package id.bima.diver.notebookshop.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotebookTransactionHistoryDto implements Serializable {
	
	private static final long serialVersionUID = 5187948018758512744L;
	
	private Long id;
	private String productName;
	private String productCategory;
	private String productBrand;
	private String productType;
	private BigDecimal quantitySold;
	private BigDecimal unitPrice;
	private String unitDetail;
	private String additionalData1;
	private String additionalData2;
	private String additionalData3;
	private String additionalData4;
	private String additionalData5;
	
}
