package id.bima.diver.notebookshop.util;

import org.springframework.stereotype.Component;

import id.bima.diver.common.util.DtoUtil;
import id.bima.diver.notebookshop.dto.NotebookTransactionHistoryDto;
import id.bima.diver.notebookshop.entity.NotebookTransactionHistory;

@Component
public class NotebookTransactionHistoryDtoUtil implements DtoUtil<NotebookTransactionHistoryDto, NotebookTransactionHistory> {

	public NotebookTransactionHistoryDto constructDto(NotebookTransactionHistory entity) {
		if (entity == null) {
			return null;
		}
		
		return NotebookTransactionHistoryDto.builder()
				.id(entity.getId())
				.productName(entity.getProductName())
				.productCategory(entity.getProductCategory())
				.productBrand(entity.getProductBrand())
				.productType(entity.getProductType())
				.quantitySold(entity.getQuantitySold())
				.unitPrice(entity.getUnitPrice())
				.unitDetail(entity.getUnitDetail())
				.additionalData1(entity.getAdditionalData1())
				.additionalData2(entity.getAdditionalData2())
				.additionalData3(entity.getAdditionalData3())
				.additionalData4(entity.getAdditionalData4())
				.additionalData5(entity.getAdditionalData5())
				.build();
	}
	
	public NotebookTransactionHistory constructEntity(NotebookTransactionHistoryDto dto) {
		if (dto == null) {
			return null;
		}
		
		return NotebookTransactionHistory.builder()
				.id(dto.getId())
				.productName(dto.getProductName())
				.productCategory(dto.getProductCategory())
				.productBrand(dto.getProductBrand())
				.productType(dto.getProductType())
				.quantitySold(dto.getQuantitySold())
				.unitPrice(dto.getUnitPrice())
				.unitDetail(dto.getUnitDetail())
				.additionalData1(dto.getAdditionalData1())
				.additionalData2(dto.getAdditionalData2())
				.additionalData3(dto.getAdditionalData3())
				.additionalData4(dto.getAdditionalData4())
				.additionalData5(dto.getAdditionalData5())
				.build();
	}

}
