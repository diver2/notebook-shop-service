package id.bima.diver.notebookshop.constant;

public class NotebookShopDb {

	private NotebookShopDb() {
	}

	// Schema name
	public static final String SCHEMA = "notebook_shop";

	// Table name
	public static final String NOTEBOOK_SHOP_TRANSACTION = "t_notebook_transaction_history";

}
