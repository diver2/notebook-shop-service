package id.bima.diver.notebookshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.bima.diver.notebookshop.entity.NotebookTransactionHistory;

@Repository
public interface NotebookTransactionHistoryRepository extends JpaRepository<NotebookTransactionHistory, Long> {
	
}
