package id.bima.diver.notebookshop.entity;

import java.math.BigDecimal;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import id.bima.diver.notebookshop.constant.NotebookShopDb;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = NotebookShopDb.NOTEBOOK_SHOP_TRANSACTION, schema = NotebookShopDb.SCHEMA)
@DynamicInsert
@DynamicUpdate
public class NotebookTransactionHistory extends AuditEntity {

	private static final long serialVersionUID = -2881605343985518502L;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "product_category")
	private String productCategory;
	
	@Column(name = "product_brand")
	private String productBrand;
	
	@Column(name = "product_type")
	private String productType;
	
	@Column(name = "quantity_sold")
	private BigDecimal quantitySold;
	
	@Column(name = "unit_price")
	private BigDecimal unitPrice;
	
	@Column(name = "unit_detail")
	private String unitDetail;
	
	@Column(name = "additional_data_1")
	private String additionalData1;
	
	@Column(name = "additional_data_2")
	private String additionalData2;
	
	@Column(name = "additional_data_3")
	private String additionalData3;
	
	@Column(name = "additional_data_4")
	private String additionalData4;
	
	@Column(name = "additional_data_5")
	private String additionalData5;

}
